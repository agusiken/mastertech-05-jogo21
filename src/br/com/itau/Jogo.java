package br.com.itau;

import java.util.List;
import java.util.Scanner;

public class Jogo {
    private Baralho baralho = new Baralho();
    private Carta carta = new Carta();
    private int pontuacao = 0;
    private boolean jogoEncerrado;
    private boolean jogoGanho;
    private boolean continuarJogo;
    private boolean decideJogo = true;
    Scanner scanner = new Scanner(System.in);

    public List<Carta> inicioJogo(){
        List<Carta> cartasInicio = baralho.sortear(2);

        for(Carta carta : cartasInicio){
            pontuacao += carta.getNumero().valor;
        }
        mostrarSituacao();


        return cartasInicio;
    }

    public Carta jogar() {

        while(!jogoEncerrado) {
            continuarJogo();

            if (decideJogo && jogoEncerrado == false) {
                Carta carta = baralho.sortear();

                pontuacao += carta.getNumero().valor;
                verificarPontuacao();
            }
            mostrarSituacao();

            if (jogoEncerrado) {
                System.out.println("Jogo encerrado!");
                decideJogo = false;
                if (jogoGanho) {
                    System.out.print("Parabéns, você ganhou!");
                }else{
                    System.out.print("Você perdeu!");
                }
            }
        }

        return carta;
    }

    public void mostrarSituacao(){
        for(Carta cartaSelecionada : baralho.getCartasSelecionadas()){
            System.out.print(cartaSelecionada.toString() + " | ");
        }
        System.out.println("\nPontuacao: " + pontuacao);
    }

    public void verificarPontuacao() {
        if (pontuacao >= 21) {
            jogoEncerrado = true;

            if (pontuacao == 21) {
                jogoGanho = true;
            } else {
                jogoGanho = false;
            }
        }
    }

    public boolean continuarJogo(){
        System.out.println("Deseja continuar: ");
        System.out.println("1. Sim"
                          +" | 2. Não");
        int opcao = scanner.nextInt();
        do {
            switch (opcao) {
                case 1:
                    decideJogo = true;
                    break;
                case 2:
                    decideJogo = false;
                    jogoEncerrado = true;
                    break;
                default:
                    System.out.println("Opcao inválida!");
            }
        }while(opcao != 1 && opcao != 2);

        return decideJogo;
    }
}