package br.com.itau;

import java.util.List;
import java.util.Scanner;

public class Main {

    public static void main(String[] args) {
        Jogo jogo = new Jogo();
        Baralho baralho = new Baralho();

        jogo.inicioJogo();
        jogo.jogar();

    }
}
