package br.com.itau;

public class Carta {
    private Naipe naipe;
    private Numero numero;

    public Carta(){ }

    public Carta(Naipe naipe, Numero numero) {
        this.naipe = naipe;
        this.numero = numero;
    }

    public Naipe getNaipe() {
        return naipe;
    }

    public Numero getNumero() {
        return numero;
    }

    public String toString(){
        return ("Carta " + numero.toString() + " de " + naipe.toString());
    }
}