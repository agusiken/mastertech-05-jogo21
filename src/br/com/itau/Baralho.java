package br.com.itau;

import java.util.ArrayList;
import java.util.List;
import java.util.Random;

public class Baralho {
    List<Carta> cartasBaralho = new ArrayList<>();
    List<Carta> cartasSelecionadas = new ArrayList<>();
    List<Carta> cartasIniciais = new ArrayList<>();
    int qtdBaralho;
    Random random;

    public List<Carta> getCartasSelecionadas() {
        return cartasSelecionadas;
    }

    public Baralho() {
        for (Naipe naipe : Naipe.values()) {
            for (Numero numero : Numero.values()) {
                cartasBaralho.add(new Carta(naipe, numero));
            }
        }
        qtdBaralho = cartasBaralho.size();
    }

    public Carta sortear(){
        int posicao = (int) (Math.random() * cartasBaralho.size());

        cartasSelecionadas.add(cartasBaralho.get(posicao));
        return cartasBaralho.remove(posicao);
    }

    public List<Carta> sortear(int qtd){
        for(int i = 0; i < qtd; i++){
            sortear();
        }
        return cartasSelecionadas;
    }
}